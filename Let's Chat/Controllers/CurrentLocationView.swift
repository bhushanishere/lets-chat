//
//  CurrentLocationView.swift
//  Let's Chat
//
//  Created by Bhushan  Borse on 11/02/20.
//  Copyright © 2020 Bhushan  Borse. All rights reserved.
//

import UIKit
//import Alamofire
import CoreLocation

class CurrentLocationView: UIViewController, CLLocationManagerDelegate {
    
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var textViewer: UITextView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var testButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("viewDidLoad called")
        
        loader.hidesWhenStopped = true
        self.testButton.isEnabled = false
        self.testButton.isUserInteractionEnabled = false
        
        
        (UIApplication.shared.delegate as! AppDelegate).restrictRotation = .portrait
        
//        if UrlParameters.recipientId == "" || UrlParameters.senderId == "" || UrlParameters.recipientName == "" {
//            let alert = UIAlertController(title: "Opps...", message: "Message", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
//                print("default clicked")
//            }))
//            self.present(alert, animated: true, completion: nil)
//        }
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadUIOnUrlScheme()
    }
    
    
    func loadUIOnUrlScheme(){
        
        loader.startAnimating()
        textViewer.text = "Fetching location..."
        testButton.isEnabled = false;
        testButton.isUserInteractionEnabled = false;
        
        print("viewDidAppear called 1")
        print(UrlParameters.recipientName)
        print(UrlParameters.recipientId)
        print(UrlParameters.senderId)
        if UrlParameters.recipientId == "1" || UrlParameters.senderId == "1" || UrlParameters.recipientName == "1" {
            let alert = UIAlertController(title: "Opps...", message: "You can not access this feature directly.\nPlease checkin through your bot.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                print("default clicked")
            }))
            self.present(alert, animated: true, completion: nil)
            loader.stopAnimating()
            textViewer.text = "If this app does not close automatically, then please close this app manually or press back."
        } else {
            self.locationManager.requestAlwaysAuthorization()
            self.locationManager.requestWhenInUseAuthorization()
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.requestLocation()
                //            startUpdatingLocation()
            }
        }
    }
    
    
    @IBAction func testButton(_ sender: Any) {
        
        ///TODO :  Commenting for now
        
        
        /// TODO : As per requirment i changed the view controller.
        /*let mapVC = storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        mapVC.latitude = UrlParameters.lat
        mapVC.longitude = UrlParameters.lon
        self.navigationController?.pushViewController(mapVC, animated: true)*/
        
        /// Get Current Location or custome location and send Lat, Long to server.
        let viewController = LocationPickerController(success: {
            [weak self] (coordinate: CLLocationCoordinate2D) -> Void in
            /*self?.locationLabel.text = "".appendingFormat("%.4f, %.4f",
                coordinate.latitude, coordinate.longitude)*/
            
            UrlParameters.lat = coordinate.latitude
            UrlParameters.lon = coordinate.longitude
            
            self?.sendCordinatesToBots(UrlParameters.lat, UrlParameters.lon)
            
            /// Added new WebHook API...
            self?.callWebhookApiWith(UrlParameters.lat, UrlParameters.lon)
            
            self?.textViewer.text = "Latitude: \(coordinate.latitude) \nLongitude: \(coordinate.longitude)"

            })
        let navigationController = UINavigationController(rootViewController: viewController)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    //Updated location
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.first != nil {
            print("abcd")
        }
        let location = locations.last! as CLLocation
        
        UrlParameters.lat = location.coordinate.latitude
        UrlParameters.lon = location.coordinate.longitude
        
        loader.stopAnimating();
        textViewer.text = "Latitude: \(UrlParameters.lat) \nLongitude: \(UrlParameters.lon)"
        
        self.testButton.isEnabled = true
        self.testButton.isUserInteractionEnabled = true
    }
    
    func sendCordinatesToBots(_ lat: Double, _ long: Double) {
        print("called sendCordinatesToBots with value lat : \(lat)  long :  \(long)")
        
        print("FETCHED : \(UrlParameters.recipientId)")
        
        print("ABCD \(UrlParameters.dateInLong)")
        var data = [
            "object": "page",
            "entry": [
                [
                    "id": "\(UrlParameters.senderId)",
                    "time": UrlParameters.dateInLong,
                    "messaging": [
                        [
                            "sender": [
                                "id": "\(UrlParameters.recipientId)"
                            ],
                            "recipient": [
                                "id": "\(UrlParameters.senderId)"
                            ],
                            "timestamp": UrlParameters.dateInLong,
                            "message": [
                                "mid": "FAwhhQCaPldUQVCS5KlJeh8jjQGQdROs_ub4no9qI2eVXUuG7tRfJW_JXWruvfDYIZ8gZXnT_WfbLSwr1aVP5A",
                                "seq": 0,
                                "attachments": [
                                    [
                                        "title": "\(UrlParameters.recipientName)'s Location",
                                        "url": "https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.bing.com%2Fmaps%2Fdefault.aspx%3Fv%3D2%26pc%3DFACEBK%26mid%3D8100%26where1%3D17.00761201793%252C%2B81.792246624904%26FORM%3DFBKPL1%26mkt%3Den-US&h=AT2WNHvkjn3rdov0MziFUnVvoSwsiHM17ZWW57I7H6Mlce1Llkg0jw-ziPxgUDMjtq6gS4_seM05QrLAkmpBnbmAfau4x9U1Dms32MkGBbf_4kEvByagTSYPqsvWcJas3ZJTee6-LbfG8so&s=1",
                                        "type": "location",
                                        "payload": [
                                            "coordinates": [
                                                "lat": UrlParameters.lat,
                                                "long": UrlParameters.lon
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
            ] as [String : Any]
        
        
        var urlString = "https://bots1.quickwork.co/QuickworkPMSBot/fbHandler"
        
        if UrlParameters.callbackURL != nil {
            urlString = UrlParameters.callbackURL ?? "https://bots1.quickwork.co/QuickworkPMSBot/fbHandler";
        }

        WebServiceClass().postData(data, url: urlString) { (response, error) in
           if let httpResponse = response as? HTTPURLResponse {
                if let nScore = Optional(httpResponse.statusCode ) , nScore == 204 || nScore == 200 {
                    DispatchQueue.main.async {
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                    /// As per requirment i commented this code...
                   /*
                        self.textViewer.text = "Your location has been captured.\nIf this app does not close automatically, then please close this app manually or press back."
                        self.testButton.isEnabled = false
                        self.testButton.isUserInteractionEnabled = false
                    }*/
                   
                } else {
                    DispatchQueue.main.async {
                        self.textViewer.text = "Sorry! Could not complete your action.\nPlease try again later."
                    }
                }
            }
        }
        
        

        ///TODO : Old Code.... commented due to Almofire pod not working.....
     /*   AF.request(urlString, method: .post, parameters: data,encoding: JSONEncoding.default, headers: nil).responseString { response in
            
            if let nScore = Optional(response.response?.statusCode ?? 0 ) , nScore == 204 || nScore == 200 {
                self.textViewer.text = "Your location has been captured.\nIf this app does not close automatically, then please close this app manually or press back."
                self.testButton.isEnabled = false
                self.testButton.isUserInteractionEnabled = false
            } else {
                self.textViewer.text = "Sorry! Could not complete your action.\nPlease try again later."
            }
        }
            */
    }
    
    func callWebhookApiWith(_ lat: Double, _ long: Double) {
        
         let param = [
          "message_type": "incoming",
          "content_type": 7,
          "payload": [
            "coordinates": [
              "lat": lat,
              "long": long
            ],
            "qw-status": "success",
            "qw-platform": "iOS",
            "qw-message": "working"
          ]
        ] as [String : Any]
        
        print(param)
        
        WebServiceClass().postData(param, url: webhookApi) { (response, error) in
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 200 {

                    /// Got success.......
                } else {
                    
                    /// Got Error.....
                }
            }
        }
    }
}

struct UrlParameters {
    static var recipientId:String = ""
    static var senderId:String = ""
    static var recipientName:String = ""
    static var callbackURL:String?
    static var lat = Double()
    static var lon = Double()
    static var dateInLong = Date().timeIntervalSince1970
}
