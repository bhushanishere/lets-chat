//
//  ChatViewController.swift
//  Let's Chat
//
//  Created by Bhushan  Borse on 08/02/20.
//  Copyright © 2020 Bhushan  Borse. All rights reserved.
//

import UIKit
import WebKit
import CoreLocation
import SafariServices

var globalVariables = GlobalVaribales();

class ChatViewController: UIViewController, WKScriptMessageHandler, WKNavigationDelegate, UIScrollViewDelegate {

    @IBOutlet weak var webKitView   : WKWebView!
    var locationManager             = CLLocationManager()
    var callFirstTime               = true

    override func loadView() {

        super.loadView()

        let userContentController = WKUserContentController()
        userContentController.add(self, name: "buttonClickPAss")
        userContentController.add(self, name: "storeuserid")
        userContentController.add(self, name: "getAuthTokenMethod")

        let config = WKWebViewConfiguration()
        config.userContentController = userContentController
        config.dataDetectorTypes = [.all]
        config.dataDetectorTypes = .phoneNumber
        config.dataDetectorTypes = .phoneNumber
        webKitView = WKWebView(frame: .zero, configuration: config)
        // webKitView = WKWebView(frame: self.webKitView.bounds, configuration: config)
        webKitView.scrollView.delegate = self
        self.view = self.webKitView!
        userContentController.add(self,name: "nativeProcess")
        userContentController.add(self,name: "sendCookie")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Chat View"
        self.loadView()
        webKitView.navigationDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadHtmlFile()
    }

    func loadHtmlFile() {
        let urlString = Bundle.main.url(forResource: "HTMLScript", withExtension:"html")
        //let urlString = Bundle.main.url(forResource: "OLDHTMLScript", withExtension:"html")
       // let urlString = "https://hubliteamtestnew.s3.ap-south-1.amazonaws.com/ios/test.html"
        let request = NSURLRequest(url: urlString!)
        webKitView.load(request as URLRequest)
    }

    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print("-----------------------------------------")
               print(message.body);
        print("-----------------------------------------")
        
       // print("Message received: \(message.name) with body: \(message.body)");
        
        /// If user Clicked on Location Button then open location view...
        if message.name == "location clicked"{
            openCurrentLocationView()
        }

        /// If having Token in UserDefault then call api with old Token or store new Token...
        if (UserDefaults.standard.value(forKey: Stored.cookie.rawValue) == nil) {
            callStoreAuthTokenMethod()
        } else if self.callFirstTime {
            replaceCookiesWithOldAndCallApi()
        }
    }

    func callStoreAuthTokenMethod() {
        let script =  "getAuthToken()"
        self.webKitView.evaluateJavaScript(script) { (data, error) in
            
            guard let dict = data as? [AnyHashable: Any] else {
                print("\(String(describing: data)) couldn't be converted to Dictionary")
                return
            }
                        
           self.storedDataInUserDefault(data: dict)
        }
    }
    
    func storedDataInUserDefault(data : [AnyHashable : Any]) {
        let cookiedString  = data[Stored.cookie.rawValue] as! String
        let newString = cookiedString.replacingOccurrences(of: "cw_conversation=", with: "")
        UserDefaults.standard.setValue(newString, forKey: Stored.cookie.rawValue)
        UserDefaults.standard.setValue(data[Stored.referer_url.rawValue], forKey: Stored.referer_url.rawValue)
        UserDefaults.standard.setValue(data[Stored.test.rawValue], forKey: Stored.test.rawValue)
    }
    
    func replaceCookiesWithOldAndCallApi() {
        self.callFirstTime = false
        let cookies =  UserDefaults.standard.string(forKey: Stored.cookie.rawValue)
        webKitView.evaluateJavaScript("checkToken(\'"+cookies!+"\')", completionHandler: nil)
    }
    
    
    
    /*func test(){
        let test = globalVariables.getAuthToken();
        webKitView.evaluateJavaScript("checkToken(\'"+test+"\')", completionHandler: nil)
    }

    func saveAuthToken(authToken:String,websiteToken:String,referer_url:String){
        globalVariables.setWebsiteToken(websiteToken: websiteToken);
        globalVariables.setReferer_url(referer_url: referer_url);
        globalVariables.setAuthToken(authToken: authToken);
    }

    func sendCookie(cookie:String) {
        print(cookie);
    }
    */
    
    func openCurrentLocationView()  {
        let vcObject = storyboard?.instantiateViewController(withIdentifier: "CurrentLocationView") as! CurrentLocationView
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.navigationController?.pushViewController(vcObject, animated: true)
    }
    
    // MARK:- Get User Current Location
    func getUserCurrentLocation() {
        
        let status  = CLLocationManager.authorizationStatus()
        if status == .notDetermined {
            self.locationManager.requestWhenInUseAuthorization()
        }
        
        if status == .denied || status == .restricted {
            showAlertView(message: "Please Enable Location Services in Settings", viewController: self)
            return
        }
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        //MARK -  Get current user location
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.delegate = self
            self.locationManager.startUpdatingLocation()
            
            let scriptSource = "locationCallback()"
            self.webKitView.evaluateJavaScript(scriptSource) { (data, error) in
                print("data \(String(describing: data))")
                print("error \(String(describing: error))")
            }
            
            self.webKitView.evaluateJavaScript("navigator.userAgent", completionHandler: { result, error in
                if let userAgent = result as? String {
                    print(userAgent)
                }
            })
        }
    }
}


extension ChatViewController : CLLocationManagerDelegate {
    // MARK:- Location Manager Delegate Methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else {
            return
        }
        //print("locations = \(locValue.latitude) \(locValue.longitude)")
      /*  latitudeObj = String(locValue.latitude)
        longitudeObj = String(locValue.longitude)
        
        print("user latitude = \(latitudeObj)")
        print("user longitude = \(longitudeObj)")*/
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("locationManager didFailWithError \(error)")
    }
}



class GlobalVaribales {

    var authToken:String = ""
    var websiteToken:String = ""
    var timestamp:String = ""
    var referer_url:String = ""

    func  getAuthToken() -> String{
        return authToken;
    }

    func setAuthToken(authToken:String) {
        self.authToken = authToken;
    }

    public func getWebsiteToken()-> String{
        return websiteToken;
    }

    func setWebsiteToken(websiteToken:String){
        self.websiteToken = websiteToken;
    }

    func getReferer_url() -> String{
        return referer_url;
    }

    func setReferer_url(referer_url:String){
        self.referer_url = referer_url;
    }

    func getTimestamp() -> String{
        return timestamp;
    }

    func setTimestamp(timestamp:String){
        self.timestamp = timestamp;
    }

    func setDefaultParams(){
        self.authToken = "eyJhbGciOiJIUzI1NiJ9.eyJzb3VyY2VfaWQiOiI3ZTJkOTg1NS02OWQ5LTRkZDgtOGUzNS04MTM0YzRlODE2YjQiLCJpbmJveF9pZCI6NX0.WTbOA1cUMicgHZp2__MrZZTka59fsynolTBw11zRi-M";
        self.websiteToken = "Y1xVddkBvaJFLGL7mJJL38Ha";
        self.referer_url = "http://qarma-pms.s3-us-west-2.amazonaws.com/qc-test-script/pankajbot.html";
    }

    public func log() {
        print("authToken: "+self.authToken);
        print("websiteToken: "+self.websiteToken);
        print("timestamp: "+self.timestamp);
        print("referer_url: "+self.referer_url);
    }
    
    
    
    
}

enum Stored : String {
    case cookie
    case referer_url
    case test
}

