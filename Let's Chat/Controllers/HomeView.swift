//
//  HomeView.swift
//  Let's Chat
//
//  Created by Bhushan  Borse on 08/02/20.
//  Copyright © 2020 Bhushan  Borse. All rights reserved.
//

import UIKit

class HomeView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func letsChatButtonPressed(_ sender: UIButton) {
       let chatVC = storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.navigationController?.pushViewController(chatVC, animated: true)        
    }
}
