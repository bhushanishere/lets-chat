//
//  ModelClass.swift
//  Let's Chat
//
//  Created by Bhushan  Borse on 08/02/20.
//  Copyright © 2020 Bhushan  Borse. All rights reserved.
//

import UIKit
import Foundation

/// Url's
let chatHTMLUrl = "https://hubliteamtestnew.s3.ap-south-1.amazonaws.com/ios/test.html"

let webhookApi = "https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.bing.com%2Fmaps%2Fdefault.aspx%3Fv%3D2%26pc%3DFACEBK%26mid%3D8100%26where1%3D17.00761201793%252C%2B81.792246624904%26FORM%3DFBKPL1%26mkt%3Den-US&h=AT2WNHvkjn3rdov0MziFUnVvoSwsiHM17ZWW57I7H6Mlce1Llkg0jw-ziPxgUDMjtq6gS4_seM05QrLAkmpBnbmAfau4x9U1Dms32MkGBbf_4kEvByagTSYPqsvWcJas3ZJTee6-LbfG8so&s=1"



















// MARK:- Show AlertView
func showAlertView(message : String, viewController: UIViewController) {

    let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
    viewController.present(alert, animated: true, completion: nil)
}

// MARK:- Show new View Controller
func pushViewController(to : UIViewController, fromViewController: UIViewController) {
    let newViewController = to
    fromViewController.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    fromViewController.navigationController?.pushViewController(newViewController, animated: true)
}


