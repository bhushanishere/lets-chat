//
//  Extensions.swift
//  Let's Chat
//
//  Created by Bhushan  Borse on 11/02/20.
//  Copyright © 2020 Bhushan  Borse. All rights reserved.
//

import UIKit
import Foundation

public extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get { return layer.cornerRadius }
        set { layer.cornerRadius = newValue }
    }
}
