//
//  CurrentLocationView.swift
//  Let's Chat
//
//  Created by Bhushan  Borse on 11/02/20.
//  Copyright © 2020 Bhushan  Borse. All rights reserved.
//

import Foundation
//MARK: ------------------> Basic Url Here <------------------

//var basicUrl = domainUrl + "StellarPOSAPI.svc/"

class WebServiceClass: NSObject {

    /// Call Globle funtion
    func postData(_ params : Dictionary<String, Any>, url : String, postCompleted : @escaping (_ urlResponse:URLResponse?,_ error:NSError?) -> ()) {
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
        request.timeoutInterval = 150
        let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            guard data != nil else {
                print("no data found: \(error!)")
                postCompleted(response,error as NSError?)
                return
            }
            postCompleted(response,error as NSError?)
        })
        task.resume()
    }
}
